package cat.itb.ShopDAOS.DAO.proveidor;


import cat.itb.ShopDAOS.DAO.comanda.Comanda;
import cat.itb.ShopDAOS.DAO.comanda.ComandaDAO;
import cat.itb.ShopDAOS.DAO.comanda.ComandaImpSQL;
import cat.itb.ShopDAOS.DAOFactory.DAOFactory;
import cat.itb.ShopDAOS.utils.Printer;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.Serializable;


public class Proveidor implements Serializable, PropertyChangeListener {

    private int id;
    private String nom;
    private String adreca;
    private String ciutat;
    private String estat;
    private String codiPostal;
    private int area;
    private String telefon;
    private int producteId;
    private int quantitat;
    private int limitCredit;
    private String observacions;
    // constructors


    public Proveidor(int id, String nom, String adreca, String ciutat, String estat, String codiPostal, int area, String telefon, int producteId, int quantitat, int limitCredit, String observacions) {
        this.id = id;
        this.nom = nom;
        this.adreca = adreca;
        this.ciutat = ciutat;
        this.estat = estat;
        this.codiPostal = codiPostal;
        this.area = area;
        this.telefon = telefon;
        this.producteId = producteId;
        this.quantitat = quantitat;
        this.limitCredit = limitCredit;
        this.observacions = observacions;
    }
    // listeners
    public void propertyChange(PropertyChangeEvent evt) {
        System.out.printf("Stock anterior: %d%n", (int) evt.getOldValue());
        System.out.printf("Stock actual: %d%n", (int) evt.getNewValue());
        ProveidorImpSQL sqlDao = (ProveidorImpSQL) DAOFactory.getDAOFactory(DAOFactory.POSTGRESQL).getProveidorDAO();
        ProveidorDAO mongoDao = DAOFactory.getDAOFactory(DAOFactory.MONGODB).getProveidorDAO();
        ProveidorDAO jsonDao = DAOFactory.getDAOFactory(DAOFactory.JSON).getProveidorDAO();
        //
        boolean isSqlAdded = sqlDao.modificarQuantitat(this);
        boolean isMongoAdded = mongoDao.modificarQuantitat(this);
        jsonDao.insertarLlista(sqlDao.consultarLlista());
        Printer.printResult("Proveidor JsonFile updated");
        if(isSqlAdded ) {
            Printer.printResult(" quantiat Proveidor updated into sql ddbb");
            Printer.printData(this.toString());
        } else Printer.printError("error trying update Proveidor en sql ddbb");
        if( isMongoAdded ) {
            Printer.printResult(" quantiat Proveidor updated into mongo ddbb");
            Printer.printData(this.toString());
        } else Printer.printError("error trying update Proveidor en mongo ddbb");
    }
    // gets and sets

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAdreca() {
        return adreca;
    }

    public void setAdreca(String adreca) {
        this.adreca = adreca;
    }

    public String getCiutat() {
        return ciutat;
    }

    public void setCiutat(String ciutat) {
        this.ciutat = ciutat;
    }

    public String getEstat() {
        return estat;
    }

    public void setEstat(String estat) {
        this.estat = estat;
    }

    public String getCodiPostal() {
        return codiPostal;
    }

    public void setCodiPostal(String codiPostal) {
        this.codiPostal = codiPostal;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public int getProducteId() {
        return producteId;
    }

    public void setProducteId(int producteId) {
        this.producteId = producteId;
    }

    public int getQuantitat() {
        return quantitat;
    }

    public void setQuantitat(int quantitat) {
        this.quantitat = quantitat;
    }

    public int getLimitCredit() {
        return limitCredit;
    }

    public void setLimitCredit(int limitCredit) {
        this.limitCredit = limitCredit;
    }

    public String getObservacions() {
        return observacions;
    }

    public void setObservacions(String observacions) {
        this.observacions = observacions;
    }

    @Override
    public String toString() {
        return "Proveidor{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", adreca='" + adreca + '\'' +
                ", ciutat='" + ciutat + '\'' +
                ", estat='" + estat + '\'' +
                ", codiPostal='" + codiPostal + '\'' +
                ", area=" + area +
                ", telefon='" + telefon + '\'' +
                ", producteId=" + producteId +
                ", quantitat=" + quantitat +
                ", limitCredit=" + limitCredit +
                ", observacions='" + observacions + '\'' +
                '}';
    }
}
