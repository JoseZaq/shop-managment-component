package cat.itb.ShopDAOS.DAO.proveidor;

import cat.itb.ShopDAOS.DAO.empleat.Empleat;
import cat.itb.ShopDAOS.DAOFactory.JsonDbDAOFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class ProveidorImpFile implements ProveidorDAO{
    static final String FILE_NAME = "proveidors.json";
    private final Type listType = new TypeToken<List<Proveidor>>() {}.getType();
    File file;
    public ProveidorImpFile() {
        file = new File("./"+ JsonDbDAOFactory.FOLDER_NAME+"/"+FILE_NAME);
    }
    @Override
    public boolean insertar(Proveidor prov) {
        return false;
    }

    @Override
    public int insertarLlista(List<Proveidor> provs) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        try(FileWriter fileWriter = new FileWriter(file);) {
            gson.toJson(provs, listType,fileWriter); // serialize and save to file
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return provs.size();
    }

    @Override
    public boolean eliminar(int idProv) {
        return false;
    }

    @Override
    public boolean eliminarConjunt() {
        return false;
    }

    @Override
    public boolean modificarQuantitat(Proveidor prov) {
        return false;
    }

    @Override
    public Proveidor consultar(int idProv) {
        return null;
    }

    @Override
    public List<Proveidor> consultarLlista() {
        List<Proveidor> proveidors = new ArrayList<>();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        try(Reader reader = Files.newBufferedReader(file.toPath())) {
            proveidors = gson.fromJson(reader, listType); // read
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return proveidors;
    }

    @Override
    public Proveidor consultarPerIdProducte(int productID) {
        return null;
    }
}
