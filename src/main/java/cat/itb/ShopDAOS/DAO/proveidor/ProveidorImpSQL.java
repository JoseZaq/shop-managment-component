package cat.itb.ShopDAOS.DAO.proveidor;

import cat.itb.ShopDAOS.DAO.empleat.Empleat;
import cat.itb.ShopDAOS.DAO.producte.Producte;
import cat.itb.ShopDAOS.DAOFactory.PsqlDbDAOFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProveidorImpSQL implements ProveidorDAO{
    Connection conexion;

    public ProveidorImpSQL() { conexion = PsqlDbDAOFactory.crearConexion();}
    @Override
    public boolean insertar(Proveidor prov) {
        return false;
    }

    @Override
    public int insertarLlista(List<Proveidor> provs) {
        return 0;
    }

    @Override
    public boolean eliminar(int idProv) {
        return false;
    }

    @Override
    public boolean eliminarConjunt() {
        return false;
    }

    @Override
    public boolean modificarQuantitat(Proveidor prov) {
        //construir orden INSERT
        String sql = "UPDATE prov SET quantitat= ? WHERE id_prov = ?";
        try {
            PreparedStatement ps = conexion.prepareStatement(sql);
            ps.setInt(2,prov.getId());
            ps.setInt(1,prov.getQuantitat());
            ps.execute();
            return true;
        } catch (SQLException e) {
            //e.printStackTrace();
            System.out.printf("HA OCURRIDO UNA EXCEPCI�N:%n");
            System.out.printf("Mensaje   : %s %n", e.getMessage());
            System.out.printf("SQL estado: %s %n", e.getSQLState());
            System.out.printf("C�d error : %s %n", e.getErrorCode());
        }
        return false;
    }

    @Override
    public Proveidor consultar(int idProv) {
        Proveidor prod = null;
        try {

            String sql = "SELECT * FROM prov WHERE id_prov = ?";
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setInt(1,idProv);
            sentencia.execute();
            ResultSet rs = sentencia.getResultSet();
            while(rs.next()) {
                String nom = rs.getString("nom");
                String adreca = rs.getString("adreca");
                String ciutat = rs.getString("ciutat");
                String estat = rs.getString("estat");
                String codiPostal = rs.getString("codi_postal");
                int area = rs.getInt("area");
                String telefon = rs.getString("telefon");
                int provId = rs.getInt("id_prov");
                int quantitat = rs.getInt("quantitat");
                int limitCredit = rs.getInt("limit_credit");
                String observacions = rs.getString("observacions");
                prod = new Proveidor(idProv, nom, adreca, ciutat, estat,
                        codiPostal, area, telefon, provId, quantitat,
                        limitCredit, observacions);
            }
            rs.close();
            return prod;
        } catch (SQLException e) {
            System.out.println("Connection failure.");
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Proveidor> consultarLlista() {
        List<Proveidor> proveidors = new ArrayList<>();
        try {

            String sql = "SELECT * FROM prov";
            Statement sentencia = conexion.createStatement();
            boolean valor = sentencia.execute(sql);
            if (valor) {
                ResultSet rs = sentencia.getResultSet();
                while(rs.next()) {
                    int id = rs.getInt("id_prov");
                    String nom = rs.getString("nom");
                    String adreca = rs.getString("adreca");
                    String ciutat = rs.getString("ciutat");
                    String estat = rs.getString("estat");
                    String codiPostal = rs.getString("codi_postal");
                    int area = rs.getInt("area");
                    String telefon = rs.getString("telefon");
                    int provId = rs.getInt("id_prov");
                    int quantitat = rs.getInt("quantitat");
                    int limitCredit = rs.getInt("limit_credit");
                    String observacions = rs.getString("observacions");
                    Proveidor proveidor = new Proveidor(id, nom, adreca, ciutat, estat,
                            codiPostal, area, telefon, provId, quantitat,
                            limitCredit, observacions);
                    proveidors.add(proveidor);
                }
                rs.close();
                return proveidors;
            } else {
                int f = sentencia.getUpdateCount();
                System.out.printf("Filas afectadas:%d %n", f);
            }
            sentencia.close();
            return null;
        } catch (SQLException e) {
            System.out.println("Connection failure.");
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Proveidor consultarPerIdProducte(int productID) {
        return null;
    }
}
