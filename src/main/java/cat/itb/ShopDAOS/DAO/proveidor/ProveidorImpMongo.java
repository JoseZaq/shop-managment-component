package cat.itb.ShopDAOS.DAO.proveidor;

import cat.itb.ShopDAOS.DAO.empleat.Empleat;
import cat.itb.ShopDAOS.DAOFactory.MongoDbDAOFactory;
import cat.itb.ShopDAOS.utils.Printer;
import com.google.gson.Gson;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.InsertManyResult;
import org.bson.Document;

import java.util.List;

import static cat.itb.ShopDAOS.DAOFactory.MongoDbDAOFactory.CODEC_REGISTRY;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.set;

public class ProveidorImpMongo implements ProveidorDAO{
    MongoDatabase mongoDB;
    String collectionName;

    public ProveidorImpMongo() {
        mongoDB = MongoDbDAOFactory.crearConexion();
        collectionName = "proveidors";
    }

    @Override
    public boolean insertar(Proveidor prov) {
        return false;
    }

    @Override
    public int insertarLlista(List<Proveidor> provs) {
        MongoDatabase db = mongoDB.withCodecRegistry(CODEC_REGISTRY);
        MongoCollection<Proveidor> collection = db.getCollection(collectionName, Proveidor.class);
        InsertManyResult result = collection.insertMany(provs);
        Printer.printResult("MONGODB:Proveidors succesfully inserted!");
        Printer.printResult("MONGODB:# objects inserted: "+result.getInsertedIds().size()+"\n");
        return result.getInsertedIds().size();
    }

    @Override
    public boolean eliminar(int idProv) {
        return false;
    }

    @Override
    public boolean eliminarConjunt() {
        return false;
    }

    @Override
    public boolean modificarQuantitat(Proveidor prov) {
        MongoDatabase db = mongoDB;
        MongoCollection<Document> collection = db.getCollection(collectionName);
        collection.updateOne(eq("_id",prov.getId()),set("quantitat",prov.getQuantitat()));
        return true;
    }

    @Override
    public Proveidor consultar(int idProv) {
        return null;
    }

    @Override
    public List<Proveidor> consultarLlista() {
        return null;
    }

    @Override
    public Proveidor consultarPerIdProducte(int productID) {
        return null;
    }
}
