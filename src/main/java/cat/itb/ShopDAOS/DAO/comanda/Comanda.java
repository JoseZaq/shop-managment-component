package cat.itb.ShopDAOS.DAO.comanda;

import cat.itb.ShopDAOS.DAOFactory.DAOFactory;
import cat.itb.ShopDAOS.utils.Printer;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.sql.Date;

public class Comanda implements Serializable, PropertyChangeListener {

    private int id;
    private int producteId;
    private Date dataComanda;
    private int quantitat;
    private int provId;
    private Date dataTramesa;
    private int total;
    // constructor
    public Comanda(int id, int producteId, Date dataComanda, int quantitat, int provId, Date dataTramesa, int total) {
        this.id = id;
        this.producteId = producteId;
        this.dataComanda = dataComanda;
        this.quantitat = quantitat;
        this.provId = provId;
        this.dataTramesa = dataTramesa;
        this.total = total;
    }
    // listeners
    public void propertyChange(PropertyChangeEvent evt) {
        System.out.printf("Stock anterior: %d%n", (int) evt.getOldValue());
        System.out.printf("Stock actual: %d%n", (int) evt.getNewValue());
        ComandaImpSQL sqlDao = (ComandaImpSQL) DAOFactory.getDAOFactory(DAOFactory.POSTGRESQL).getComandaDAO();
        ComandaDAO mongoDao = DAOFactory.getDAOFactory(DAOFactory.MONGODB).getComandaDAO();
        ComandaDAO jsonDao = DAOFactory.getDAOFactory(DAOFactory.JSON).getComandaDAO();
        //
        Comanda newComanda = this;
        newComanda.setId(sqlDao.lastId() + 1 );
        boolean isSqlAdded = sqlDao.insertar(newComanda);
        boolean isMongoAdded = mongoDao.insertar(newComanda);
        jsonDao.insertarLlista(sqlDao.consultarLlista());
        Printer.printResult("Comanda JsonFile updated");
        if(isSqlAdded ) {
            Printer.printResult("new Comanda added into sql ddbb");
            Printer.printData(this.toString());
        } else Printer.printError("error trying to insert a new comanda en sql ddbb");
        if( isMongoAdded ) {
            Printer.printResult("new Comanda added into mongo ddbb");
            Printer.printData(this.toString());
        } else Printer.printError("error trying to insert a new comanda en mongo ddbb");
    }

    // getters and setters

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProducteId() {
        return producteId;
    }

    public void setProducteId(int producteId) {
        this.producteId = producteId;
    }

    public Date getDataComanda() {
        return dataComanda;
    }

    public void setDataComanda(Date dataComanda) {
        this.dataComanda = dataComanda;
    }

    public int getQuantitat() {
        return quantitat;
    }

    public void setQuantitat(int quantitat) {
        this.quantitat = quantitat;
    }

    public int getProvId() {
        return provId;
    }

    public void setProvId(int provId) {
        this.provId = provId;
    }

    public Date getDataTramesa() {
        return dataTramesa;
    }

    public void setDataTramesa(Date dataTramesa) {
        this.dataTramesa = dataTramesa;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "Comanda{" +
                "id=" + id +
                ", producteId=" + producteId +
                ", dataComanda=" + dataComanda +
                ", quantitat=" + quantitat +
                ", provId=" + provId +
                ", dataTramesa=" + dataTramesa +
                ", total=" + total +
                '}';
    }
}
