package cat.itb.ShopDAOS.DAO.comanda;

import cat.itb.ShopDAOS.DAOFactory.PsqlDbDAOFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ComandaImpSQL implements ComandaDAO {
    Connection connection;
    // constructor

    public ComandaImpSQL() {
        connection = PsqlDbDAOFactory.crearConexion();
    }

    // metods
    @Override
    public boolean insertar(Comanda comanda) {

        String sql = "INSERT INTO comanda VALUES(?,?,?,?,?,?,?)";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1,comanda.getId());
            ps.setInt(2,comanda.getProducteId());
            ps.setDate(3,comanda.getDataComanda());
            ps.setInt(4,comanda.getQuantitat());
            ps.setInt(5,comanda.getProvId());
            ps.setDate(6,comanda.getDataTramesa());
            ps.setInt(7,comanda.getTotal());
            ps.execute();
            return true;
        } catch (SQLException e) {
            //e.printStackTrace();
            System.out.printf("HA OCURRIDO UNA EXCEPCI�N:%n");
            System.out.printf("Mensaje   : %s %n", e.getMessage());
            System.out.printf("SQL estado: %s %n", e.getSQLState());
            System.out.printf("C�d error : %s %n", e.getErrorCode());
        }
        return false;
    }

    @Override
    public int insertarLlista(List<Comanda> comandes) {
        return 0;
    }

    @Override
    public boolean eliminar(int comandaId) {
        return false;
    }

    @Override
    public boolean eliminarConjunt() {
        return false;
    }

    @Override
    public boolean modificar(Comanda comanda) {
        //construir orden INSERT
        String sql = "UPDATE comanda SET id_producte = ?, data_comanda = ?," +
                "quantitat = ?, id_prov = ?, data_tramesa = ?, total = ? WHERE id_comanda = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(7,comanda.getId());
            ps.setInt(1,comanda.getProducteId());
            ps.setDate(2,comanda.getDataComanda());
            ps.setInt(3,comanda.getQuantitat());
            ps.setInt(4,comanda.getProvId());
            ps.setDate(5,comanda.getDataTramesa());
            ps.setInt(6,comanda.getTotal());
            ps.execute();
            return true;
        } catch (SQLException e) {
            //e.printStackTrace();
            System.out.printf("HA OCURRIDO UNA EXCEPCI�N:%n");
            System.out.printf("Mensaje   : %s %n", e.getMessage());
            System.out.printf("SQL estado: %s %n", e.getSQLState());
            System.out.printf("C�d error : %s %n", e.getErrorCode());
        }
        return false;
    }

    @Override
    public Comanda consultar(int comandaId) {
        return null;
    }

    @Override
    public List<Comanda> consultarLlistaPerProducte(int productID) {
        return null;
    }

    @Override
    public List<Comanda> consultarLlista() {
        List<Comanda> comandas = new ArrayList<>();
        try {
            String sql = "SELECT * FROM comanda";
            Statement sentencia = connection.createStatement();
            boolean valor = sentencia.execute(sql);
            if (valor) {
                ResultSet rs = sentencia.getResultSet();
                while(rs.next()) {
                    int id = rs.getInt("id_comanda");
                    int producteId = rs.getInt("id_producte");
                    Date dataComanda = rs.getDate("data_comanda");
                    int quantitat = rs.getInt("quantitat");
                    int provId = rs.getInt("id_prov");
                    Date dataTramesa = rs.getDate("data_tramesa");
                    int total = rs.getInt("total");
                    Comanda comanda = new Comanda(id, producteId, dataComanda, quantitat,
                            provId, dataTramesa, total);
                    comandas.add(comanda);
                }
                rs.close();
                return comandas;
            } else {
                int f = sentencia.getUpdateCount();
                System.out.printf("Filas afectadas:%d %n", f);
            }
            sentencia.close();
            return null;
        } catch (SQLException e) {
            System.out.println("Connection failure.");
            e.printStackTrace();
        }
        return null;
    }

    public int lastId() {
        int maxId=0;
        try {
            String sql = "select max(comanda.id_comanda) from comanda";
            Statement sentencia = connection.createStatement();
            sentencia.execute(sql);
            ResultSet rs = sentencia.getResultSet();
            while(rs.next()){
                maxId = rs.getInt("max");
            }
            sentencia.close();
            return maxId;
        } catch (SQLException e) {
            System.out.println("Connection failure.");
            e.printStackTrace();
        }
        return maxId;
    }
}
