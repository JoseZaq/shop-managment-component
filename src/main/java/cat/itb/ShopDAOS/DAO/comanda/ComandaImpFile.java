package cat.itb.ShopDAOS.DAO.comanda;

import cat.itb.ShopDAOS.DAO.empleat.Empleat;
import cat.itb.ShopDAOS.DAOFactory.JsonDbDAOFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class ComandaImpFile implements ComandaDAO{
    static final String FILE_NAME = "comandes.json";
    private final Type listType = new TypeToken<List<Comanda>>() {}.getType();
    File file;
    public ComandaImpFile() {
        file = new File("./"+ JsonDbDAOFactory.FOLDER_NAME+"/"+FILE_NAME);
    }
    @Override
    public boolean insertar(Comanda comanda) {
        return false;
    }

    @Override
    public int insertarLlista(List<Comanda> comandes) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        try(FileWriter fileWriter = new FileWriter(file);) {
            gson.toJson(comandes, listType,fileWriter); // serialize and save to file
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return comandes.size();
    }

    @Override
    public boolean eliminar(int comandaId) {
        return false;
    }

    @Override
    public boolean eliminarConjunt() {
        return false;
    }

    @Override
    public boolean modificar(Comanda comanda) {
        return false;
    }

    @Override
    public Comanda consultar(int comandaId) {
        return null;
    }

    @Override
    public List<Comanda> consultarLlistaPerProducte(int productID) {
        return null;
    }

    @Override
    public List<Comanda> consultarLlista() {
        List<Comanda> comandas = new ArrayList<>();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        try(Reader reader = Files.newBufferedReader(file.toPath())) {
            comandas = gson.fromJson(reader, listType); // read
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return comandas;
    }
}
