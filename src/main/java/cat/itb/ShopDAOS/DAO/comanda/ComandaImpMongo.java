package cat.itb.ShopDAOS.DAO.comanda;

import cat.itb.ShopDAOS.DAOFactory.MongoDbDAOFactory;
import cat.itb.ShopDAOS.utils.Printer;
import com.google.gson.Gson;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.InsertManyResult;
import com.mongodb.client.result.InsertOneResult;
import org.bson.Document;

import java.util.List;

import static cat.itb.ShopDAOS.DAOFactory.MongoDbDAOFactory.CODEC_REGISTRY;
import static com.mongodb.client.model.Filters.eq;

public class ComandaImpMongo implements ComandaDAO{
    MongoDatabase mongoDB;
    String collectionName;

    public ComandaImpMongo() {
        mongoDB = MongoDbDAOFactory.crearConexion();
        collectionName = "comandes";
    }
    @Override
    public boolean insertar(Comanda comanda) {
        MongoDatabase db = mongoDB.withCodecRegistry(CODEC_REGISTRY);
        MongoCollection<Comanda> collection = db.getCollection(collectionName, Comanda.class);
        InsertOneResult result = collection.insertOne(comanda);
        return true;
    }

    @Override
    public int insertarLlista(List<Comanda> comandes) {
        MongoDatabase db = mongoDB.withCodecRegistry(CODEC_REGISTRY);
        MongoCollection<Comanda> collection = db.getCollection(collectionName, Comanda.class);
        InsertManyResult result = collection.insertMany(comandes);
        Printer.printResult("MONGODB:Comandas succesfully inserted!");
        Printer.printResult("MONGODB:# objects inserted: "+result.getInsertedIds().size()+"\n");
        return result.getInsertedIds().size();
    }

    @Override
    public boolean eliminar(int comandaId) {
        return false;
    }

    @Override
    public boolean eliminarConjunt() {
        return false;
    }

    @Override
    public boolean modificar(Comanda comanda) {
        MongoDatabase db = mongoDB;
        MongoCollection<Document> collection = db.getCollection(collectionName);
        Gson gson = new Gson();
        collection.findOneAndReplace(eq("_id",comanda.getId()), Document.parse(gson.toJson(comanda)));
        return true;
    }

    @Override
    public Comanda consultar(int comandaId) {
        return null;
    }

    @Override
    public List<Comanda> consultarLlistaPerProducte(int productID) {
        return null;
    }

    @Override
    public List<Comanda> consultarLlista() {
        return null;
    }
}
