package cat.itb.ShopDAOS.DAO.producte;

import cat.itb.ShopDAOS.DAO.empleat.Empleat;
import cat.itb.ShopDAOS.DAOFactory.PsqlDbDAOFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProducteImpSQL implements ProducteDAO{
    Connection conexion;

    public ProducteImpSQL() { conexion = PsqlDbDAOFactory.crearConexion();
    }
    @Override
    public boolean insertar(Producte product) {
        return false;
    }

    @Override
    public int insertarLlista(List<Producte> productes) {
        return 0;
    }

    @Override
    public boolean eliminar(int productId) {
        return false;
    }

    @Override
    public boolean eliminarConjunt() {
        return false;
    }

    @Override
    public boolean modificarStock(Producte product) {
        return false;
    }

    @Override
    public Producte consultar(int productID) {
        Producte prod = null;
        try {

            String sql = "SELECT * FROM producte WHERE id_producte = ?";
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setInt(1,productID);
            sentencia.execute();
            ResultSet rs = sentencia.getResultSet();
            while(rs.next()) {
                int id = rs.getInt("id_producte");
                String descripcio = rs.getString("descripcio");
                int stockActual = rs.getInt("stockactual");
                int stockMinim = rs.getInt("stockminim");
                int preu = rs.getInt("preu");
                prod = new Producte(id, descripcio, stockActual, stockMinim, preu);
            }
            rs.close();
            return prod;
        } catch (SQLException e) {
            System.out.println("Connection failure.");
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Producte> consultarLlista() {
        List<Producte> productes = new ArrayList<>();
        try {

            String sql = "SELECT * FROM producte";
            Statement sentencia = conexion.createStatement();
            boolean valor = sentencia.execute(sql);
            if (valor) {
                ResultSet rs = sentencia.getResultSet();
                while(rs.next()) {
                    int id = rs.getInt("id_producte");
                    String descripcio = rs.getString("descripcio");
                    int stockActual = rs.getInt("stockactual");
                    int stockMinim = rs.getInt("stockminim");
                    int preu = rs.getInt("preu");
                    Producte prod = new Producte(id,descripcio, stockActual, stockMinim, preu);
                    productes.add(prod);
                }
                rs.close();
                return productes;
            } else {
                int f = sentencia.getUpdateCount();
                System.out.printf("Filas afectadas:%d %n", f);
            }
            sentencia.close();
            return null;
        } catch (SQLException e) {
            System.out.println("Connection failure.");
            e.printStackTrace();
        }
        return null;
    }
}
