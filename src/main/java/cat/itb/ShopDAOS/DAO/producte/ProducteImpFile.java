package cat.itb.ShopDAOS.DAO.producte;

import cat.itb.ShopDAOS.DAO.empleat.Empleat;
import cat.itb.ShopDAOS.DAOFactory.JsonDbDAOFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class ProducteImpFile implements ProducteDAO{
    static final String FILE_NAME = "productes.json";
    private final Type listType = new TypeToken<List<Producte>>() {}.getType();
    File file;
    public ProducteImpFile() {
        file = new File("./"+ JsonDbDAOFactory.FOLDER_NAME+"/"+FILE_NAME);
    }

    @Override
    public boolean insertar(Producte product) {
        return false;
    }

    @Override
    public int insertarLlista(List<Producte> productes) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        try(FileWriter fileWriter = new FileWriter(file);) {
            gson.toJson(productes, listType,fileWriter); // serialize and save to file
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return productes.size();
    }

    @Override
    public boolean eliminar(int productId) {
        return false;
    }

    @Override
    public boolean eliminarConjunt() {
        return false;
    }

    @Override
    public boolean modificarStock(Producte product) {
        return false;
    }

    @Override
    public Producte consultar(int productID) {
        return null;
    }

    @Override
    public List<Producte> consultarLlista() {
        List<Producte> comandes = new ArrayList<>();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        try(Reader reader = Files.newBufferedReader(file.toPath())) {
            comandes = gson.fromJson(reader, listType); // read
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return comandes;
    }
}
