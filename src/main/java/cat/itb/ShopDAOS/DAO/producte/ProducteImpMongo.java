package cat.itb.ShopDAOS.DAO.producte;

import cat.itb.ShopDAOS.DAO.empleat.Empleat;
import cat.itb.ShopDAOS.DAOFactory.MongoDbDAOFactory;
import cat.itb.ShopDAOS.utils.Printer;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.InsertManyResult;

import java.util.List;

import static cat.itb.ShopDAOS.DAOFactory.MongoDbDAOFactory.CODEC_REGISTRY;

public class ProducteImpMongo implements ProducteDAO{
    MongoDatabase mongoDB;
    String collectionName;

    public ProducteImpMongo() {
        mongoDB = MongoDbDAOFactory.crearConexion();
        collectionName = "productes";
    }
    @Override
    public boolean insertar(Producte product) {
        return false;
    }

    @Override
    public int insertarLlista(List<Producte> productes) {
        MongoDatabase db = mongoDB.withCodecRegistry(CODEC_REGISTRY);
        MongoCollection<Producte> collection = db.getCollection(collectionName, Producte.class);
        InsertManyResult result = collection.insertMany(productes);
        Printer.printResult("MONGODB:Productes succesfully inserted!");
        Printer.printResult("MONGODB:# objects inserted: "+result.getInsertedIds().size()+"\n");
        return result.getInsertedIds().size();
    }

    @Override
    public boolean eliminar(int productId) {
        return false;
    }

    @Override
    public boolean eliminarConjunt() {
        return false;
    }

    @Override
    public boolean modificarStock(Producte product) {
        return false;
    }

    @Override
    public Producte consultar(int productID) {
        return null;
    }

    @Override
    public List<Producte> consultarLlista() {
        return null;
    }
}
