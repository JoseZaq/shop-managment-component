package cat.itb.ShopDAOS.DAO.producte;

import cat.itb.ShopDAOS.DAO.comanda.Comanda;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;

public class Producte implements Serializable {
    private transient PropertyChangeSupport propertyChangeSupport;
    private int id;
    private String descripcio;
    private int stockActual;
    private int stockMinim;
    private int preu;

    // constructor

    public Producte(int id, String descripcio, int stockActual, int stockMinim, int preu) {
        this.id = id;
        this.descripcio = descripcio;
        this.stockActual = stockActual;
        this.stockMinim = stockMinim;
        this.preu = preu;
        // propertySupport
        this.propertyChangeSupport = new PropertyChangeSupport(this);
    }
    // property support methods
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.propertyChangeSupport.addPropertyChangeListener(listener);
    }
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        this.propertyChangeSupport.removePropertyChangeListener(listener);
    }

    // gets and sets


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcio() {
        return descripcio;
    }

    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }

    public int getStockActual() {
        return stockActual;
    }

    public void setStockActual(int stockActual) {
        int valorAnterior = this.stockActual;
        this.stockActual = stockActual;
        if (this.stockActual < this.getStockMinim()) {
            this.propertyChangeSupport.firePropertyChange("stockActual",
                    valorAnterior, this.stockActual);
        }
        this.stockActual = stockActual;
    }

    public int getStockMinim() {
        return stockMinim;
    }

    public void setStockMinim(int stockMinim) {
        this.stockMinim = stockMinim;
    }

    public int getPreu() {
        return preu;
    }

    public void setPreu(int preu) {
        this.preu = preu;
    }

    @Override
    public String toString() {
        return "Producte{" +
                "propertyChangeSupport=" + propertyChangeSupport +
                ", id=" + id +
                ", descripcio='" + descripcio + '\'' +
                ", stockActual=" + stockActual +
                ", stockMinim=" + stockMinim +
                ", preu=" + preu +
                '}';
    }
}
