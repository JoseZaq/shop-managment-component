package cat.itb.ShopDAOS.DAO.empleat;

import cat.itb.ShopDAOS.DAOFactory.MongoDbDAOFactory;
import cat.itb.ShopDAOS.utils.Printer;
import com.mongodb.MongoException;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.InsertManyResult;

import java.util.ArrayList;
import java.util.List;

import static cat.itb.ShopDAOS.DAOFactory.MongoDbDAOFactory.CODEC_REGISTRY;
import static com.mongodb.client.model.Filters.*;

public class EmpleatImpMongo implements EmpleatDAO {
    MongoDatabase mongoDB;
    String collectionName;

    public EmpleatImpMongo() {
        mongoDB = MongoDbDAOFactory.crearConexion();
        collectionName = "empleats";
    }


    @Override
    public boolean insertar(Empleat emps) {
        return false;
    }

    @Override
    public int insertarLlista(List<Empleat> emps) {
        MongoDatabase db = mongoDB.withCodecRegistry(CODEC_REGISTRY);
        MongoCollection<Empleat> collection = db.getCollection(collectionName, Empleat.class);
        InsertManyResult result = collection.insertMany(emps);
        Printer.printResult("MONGODB:Empleats succesfully inserted!");
        Printer.printResult("MONGODB:# objects inserted: "+result.getInsertedIds().size()+"\n");
        return result.getInsertedIds().size();
    }

    @Override
    public boolean eliminar(int empId) {
        MongoDatabase db = mongoDB.withCodecRegistry(CODEC_REGISTRY);
        MongoCollection<Empleat> collection = db.getCollection(collectionName, Empleat.class);
        // delete
        try {
            DeleteResult result  = collection.deleteOne(eq("_id",empId));
            System.out.println("#Deleted-empleats: "+result.getDeletedCount());
            return true;
        }catch (MongoException e){
            System.out.println(e.getMessage());
        }
        return false;
    }

    @Override
    public boolean eliminarConjunt() {
        return false;
    }

    @Override
    public boolean modificar(Empleat emp) {
        MongoDatabase db = mongoDB.withCodecRegistry(CODEC_REGISTRY);
        MongoCollection<Empleat> collection = db.getCollection(collectionName, Empleat.class);
        Empleat result = collection.findOneAndReplace(eq("_id",emp.getId()), emp);
        return true;
    }

    @Override
    public Empleat consultar(int empId) {
        MongoDatabase db = mongoDB.withCodecRegistry(CODEC_REGISTRY);
        MongoCollection<Empleat> collection = db.getCollection(collectionName, Empleat.class);
        // query
        return collection.find(eq("_id",empId)).first();
    }

    @Override
    public List<Empleat> consultarLlista() {
        MongoDatabase db = mongoDB.withCodecRegistry(CODEC_REGISTRY);
        MongoCollection<Empleat> collection = db.getCollection(collectionName, Empleat.class);
        List<Empleat> empleats = new ArrayList<>();
        // query
        FindIterable<Empleat> result = collection.find();
        result.forEach(empleats::add);
        return empleats;
    }
}
