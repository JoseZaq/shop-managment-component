package cat.itb.ShopDAOS.DAO.empleat;

import cat.itb.ShopDAOS.DAOFactory.JsonDbDAOFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.*;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class EmpleatImpFile implements EmpleatDAO {
    static final String FILE_NAME = "empleats.json";
    private final Type listType = new TypeToken<List<Empleat>>() {}.getType();
    File file;
    public EmpleatImpFile() {
        file = new File("./"+ JsonDbDAOFactory.FOLDER_NAME+"/"+FILE_NAME);
    }

    @Override
    public boolean insertar(Empleat empleat) {
        //gson
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setPrettyPrinting();
        Gson gson = gsonBuilder.create();
        // empleat to json
        String json = gson.toJson(empleat);
        System.out.println(json);
        // get employees from file
        List<Empleat> allEmpleats = consultarLlista();
        allEmpleats.add(empleat);
        // serialize and write
        try {
            gson.toJson(allEmpleats, listType, new FileWriter(file)); // serialize and save to file
            System.out.println("Json añadido correctamente al archivo "+file.getName()+"...");
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        // print good
        return false;
    }

    @Override
    public int insertarLlista(List<Empleat> emps) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        try(FileWriter fileWriter = new FileWriter(file);) {
            gson.toJson(emps, listType,fileWriter); // serialize and save to file
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return emps.size();
    }

    @Override
    public boolean eliminar(int empId) {
        System.out.println("No implementat");
        return false;
    }

    @Override
    public boolean eliminarConjunt() {
        System.out.println("No implementat");
        return true;
    }

    @Override
    public boolean modificar(Empleat emp) {
        System.out.println("No implementat");
        return false;
    }

    @Override
    public Empleat consultar(int empId) {
        //todo
        return null;
    }

    @Override
    public List<Empleat> consultarLlista() {
        List<Empleat> empleats = new ArrayList<>();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        try(Reader reader = Files.newBufferedReader(file.toPath())) {
            empleats = gson.fromJson(reader, listType); // read
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return empleats;
    }
}
