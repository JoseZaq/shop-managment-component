package cat.itb.ShopDAOS.DAO.empleat;

import cat.itb.ShopDAOS.DAOFactory.PsqlDbDAOFactory;

import java.sql.*;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;


public class EmpleatImpSQL implements EmpleatDAO {
    Connection conexion;

    public EmpleatImpSQL() { conexion = PsqlDbDAOFactory.crearConexion();
    }

    @Override
    public boolean insertar(Empleat emps) {
        return false;
    }

    @Override
    public int insertarLlista(List<Empleat> emps) {
        return 0;
    }

    @Override
    public boolean eliminar(int empId) {
        String query = "DELETE FROM empleat WHERE id_empleat = ?";
        try{
            PreparedStatement ps = conexion.prepareStatement(query);
            ps.setInt(1, empId);
            ps.execute();
            //close
            ps.close();
            return true;
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return false;
    }

    @Override
    public boolean eliminarConjunt() {
        return false;
    }

    @Override
    public boolean modificar(Empleat emp) {
        //construir orden INSERT
        String sql = "UPDATE empleat SET cognom = ?, ofici = ?," +
                "cap_id = ?, data_alta = ?, salari = ?, comissio = ?, dept_no = ?" +
                "WHERE id_empleat = ?";
        try {
            PreparedStatement ps = conexion.prepareStatement(sql);
            ps.setInt(8,emp.getId());
            ps.setString(1,emp.getCognom());
            ps.setString(2,emp.getOfici());
            ps.setInt(3,emp.getCapId());
            ps.setDate(4,Date.valueOf(emp.getDataAlta()));
            ps.setInt(5,emp.getSalari());
            ps.setInt(6,emp.getComissio());
            ps.setInt(7,emp.getDepNo());
            ps.execute();
            return true;
        } catch (SQLException e) {
            //e.printStackTrace();
            System.out.printf("HA OCURRIDO UNA EXCEPCI�N:%n");
            System.out.printf("Mensaje   : %s %n", e.getMessage());
            System.out.printf("SQL estado: %s %n", e.getSQLState());
            System.out.printf("C�d error : %s %n", e.getErrorCode());
        }
        return false;
    }

    @Override
    public Empleat consultar(int empId) {
        return null;
    }

    @Override
    public List<Empleat> consultarLlista() {
        List<Empleat> empleats = new ArrayList<>();
        try {

            String sql = "SELECT * FROM empleat";
            Statement sentencia = conexion.createStatement();
            boolean valor = sentencia.execute(sql);
            if (valor) {
                ResultSet rs = sentencia.getResultSet();
                while(rs.next()) {
                    int getId = rs.getInt("id_empleat");
                    String getCog = rs.getString("cognom");
                    String getOfici = rs.getString("ofici");
                    int getCapId = rs.getInt("cap_id");
                    Date getDate = rs.getDate("data_alta");
                    int getSalari = rs.getInt("salari");
                    int getComisi = rs.getInt("comissio");
                    int getDept = rs.getInt("dept_no");
                    Empleat emp = new Empleat(getId, getCog, getOfici, getCapId,
                            new java.sql.Date(getDate.getTime()).toLocalDate(),
                            getSalari, getComisi, getDept);
                    empleats.add(emp);
                }
                rs.close();
                return empleats;
            } else {
                int f = sentencia.getUpdateCount();
                System.out.printf("Filas afectadas:%d %n", f);
            }
            sentencia.close();
            return null;
        } catch (SQLException e) {
            System.out.println("Connection failure.");
            e.printStackTrace();
        }
        return null;
    }
}
