package cat.itb.ShopDAOS.DAOFactory;

import cat.itb.ShopDAOS.DAO.comanda.ComandaDAO;
import cat.itb.ShopDAOS.DAO.comanda.ComandaImpFile;
import cat.itb.ShopDAOS.DAO.empleat.EmpleatDAO;
import cat.itb.ShopDAOS.DAO.empleat.EmpleatImpFile;
import cat.itb.ShopDAOS.DAO.producte.ProducteDAO;
import cat.itb.ShopDAOS.DAO.producte.ProducteImpFile;
import cat.itb.ShopDAOS.DAO.proveidor.ProveidorDAO;
import cat.itb.ShopDAOS.DAO.proveidor.ProveidorImpFile;

import java.io.File;

public class JsonDbDAOFactory extends DAOFactory{

    public static final String FOLDER_NAME = "shopFitxersJSON";
    // contructor

    public JsonDbDAOFactory() {
        createMainFolder();

    }

    private void createMainFolder() {
        File directorio = new File("./"+ FOLDER_NAME);
        if (!directorio.exists()) {
            if (directorio.mkdirs()) {
                System.out.println("Folder created!");
            } else {
                System.out.println("Error trying to create the folder");
            }
        }
    }

    // methods
    @Override
    public ComandaDAO getComandaDAO() {
        return new ComandaImpFile();
    }

    @Override
    public EmpleatDAO getEmpleatDAO() {
        return new EmpleatImpFile();
    }

    @Override
    public ProducteDAO getProducteDAO() {
        return new ProducteImpFile();
    }

    @Override
    public ProveidorDAO getProveidorDAO() {
        return new ProveidorImpFile();
    }
}
