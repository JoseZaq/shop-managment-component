package cat.itb.ShopDAOS.DAOFactory;
import cat.itb.ShopDAOS.DAO.comanda.ComandaImpSQL;
import cat.itb.ShopDAOS.DAO.empleat.EmpleatDAO;
import cat.itb.ShopDAOS.DAO.empleat.EmpleatImpSQL;
import cat.itb.ShopDAOS.DAO.producte.ProducteImpSQL;
import cat.itb.ShopDAOS.DAO.proveidor.ProveidorDAO;
import cat.itb.ShopDAOS.DAO.comanda.ComandaDAO;
import cat.itb.ShopDAOS.DAO.producte.ProducteDAO;
import cat.itb.ShopDAOS.DAO.proveidor.ProveidorImpSQL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
public class PsqlDbDAOFactory extends DAOFactory {
    static Connection conexion = null;
//    static String DRIVER = "";
    static String URLDB = "";
    static String USUARIO = "vkeqseoo";
    static String CLAVE = "9kDqCMhd0SGjaOjn8UHlFKeCO-3R7m3Z";
    public PsqlDbDAOFactory() {
//        DRIVER = "com.mysql.jdbc.Driver";
        URLDB = "jdbc:postgresql://rogue.db.elephantsql.com/vkeqseoo"; // Ubicació de la BD.
    }
    // crear la conexion
    public static Connection crearConexion() {
        if (conexion == null) {
//            try {
//                Class.forName(DRIVER); // Cargar el driver
//            } catch (ClassNotFoundException ex) {
//                Logger.getLogger(PsqlDbDAOFactory.class.getName()).log(Level.SEVERE, null, ex);
//            }
            try {
                conexion = DriverManager.getConnection(URLDB, USUARIO, CLAVE);
            } catch (SQLException ex) {
                System.out.printf("HA OCURRIDO UNA EXCEPCIÓN:%n");
                System.out.printf("Mensaje : %s %n", ex.getMessage());
                System.out.printf("SQL estado: %s %n", ex.getSQLState());
                System.out.printf("Cód error : %s %n", ex.getErrorCode());
            }
        }
        return conexion;
    }

    @Override
    public ComandaDAO getComandaDAO() {
        return new ComandaImpSQL();
    }

    @Override
    public EmpleatDAO getEmpleatDAO() {
        return new EmpleatImpSQL();
    }

    @Override
    public ProducteDAO getProducteDAO() {
        return new ProducteImpSQL();
    }

    @Override
    public ProveidorDAO getProveidorDAO() {
        return new ProveidorImpSQL();
    }
}