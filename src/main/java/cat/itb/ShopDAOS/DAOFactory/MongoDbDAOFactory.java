package cat.itb.ShopDAOS.DAOFactory;

import cat.itb.ShopDAOS.DAO.comanda.ComandaDAO;
import cat.itb.ShopDAOS.DAO.comanda.ComandaImpMongo;
import cat.itb.ShopDAOS.DAO.empleat.EmpleatDAO;
import cat.itb.ShopDAOS.DAO.empleat.EmpleatImpMongo;
import cat.itb.ShopDAOS.DAO.producte.ProducteDAO;
import cat.itb.ShopDAOS.DAO.producte.ProducteImpMongo;
import cat.itb.ShopDAOS.DAO.proveidor.ProveidorDAO;
import cat.itb.ShopDAOS.DAO.proveidor.ProveidorImpMongo;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class MongoDbDAOFactory extends DAOFactory{
    //    static String DRIVER = "";
    static String HOST = "acceso-a-datos-mongodb.m9as2.mongodb.net/acceso-a-datos-mongoDB";
    static String USUARIO = "josezaq";
    static String CLAVE = "ITB2020239";
    static String DATABASE = "shop";
    private static MongoClient mongoClient;
    public static final CodecRegistry CODEC_REGISTRY = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
            fromProviders(PojoCodecProvider.builder().automatic(true).build()));
    public MongoDbDAOFactory() {
        mongoClient = MongoClients.create("mongodb+srv://"+USUARIO+":"+CLAVE+"@"+HOST+"?retryWrites=true&w=majority");; // Ubicació de la BD.
    }
    // crear la conexion
    public static MongoDatabase crearConexion() {
        return mongoClient.getDatabase(DATABASE);
    }

    @Override
    public ComandaDAO getComandaDAO() {
        return new ComandaImpMongo();
    }

    @Override
    public EmpleatDAO getEmpleatDAO() {
        return new EmpleatImpMongo();
    }

    @Override
    public ProducteDAO getProducteDAO() {
        return new ProducteImpMongo();
    }

    @Override
    public ProveidorDAO getProveidorDAO() {
        return new ProveidorImpMongo();
    }
}
