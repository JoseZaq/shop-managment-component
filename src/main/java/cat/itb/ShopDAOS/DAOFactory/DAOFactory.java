package cat.itb.ShopDAOS.DAOFactory;

import cat.itb.ShopDAOS.DAO.comanda.ComandaDAO;
import cat.itb.ShopDAOS.DAO.empleat.EmpleatDAO;
import cat.itb.ShopDAOS.DAO.producte.ProducteDAO;
import cat.itb.ShopDAOS.DAO.proveidor.ProveidorDAO;

public abstract class DAOFactory {
    // Bases de datos soportadas
    public static final int POSTGRESQL = 1;
    public static final int MONGODB = 2;
    public static final int JSON = 3;
    public abstract ComandaDAO getComandaDAO();
    public abstract EmpleatDAO getEmpleatDAO();
    public abstract ProducteDAO getProducteDAO();
    public abstract ProveidorDAO getProveidorDAO();
    public static DAOFactory getDAOFactory(int bd) {
        switch (bd) {
            case POSTGRESQL:
                return new PsqlDbDAOFactory();
            case MONGODB:
                return new MongoDbDAOFactory();
            case JSON:
                return new JsonDbDAOFactory();
            default :
                return null;
        }
    }
}